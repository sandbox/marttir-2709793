Biblio Wysiwyg
====================

No more typing out citekeys! This module aims to provide a plugin to insert and
edit Biblio citations much like footnotes. Verified to work with CKEditor 4.4.7.

This plugin shows a dialog to insert <bib> or <ibib> citations from the
Biblio module. It uses the fakeObjects API to show a nice icon to the user,
while producing proper tags when the text is saved or View Source is pressed.

Note: If a text contains citations in [bib]square_brackets[/bib],
nothing will be done and they will be visible in the text.


Installation
--------------------
1. Make sure you have installed and enabled the following modules: CKEditor,
   Biblio, Footnotes, Footnotes Wysiwyg.
2. Install the Views Datasource module and add patch for Autocomplete JSON
   style: https://www.drupal.org/node/2393901
3. Enable this module
3. Add the "Add citation" button to your editor. If you're using CKEditor, also
   check the "Add citation" checkbox in "Plugins".


Credits
---------------------

Based on Footnotes Wysiwyg plugin.


Known issues
----------------------

When editing an existing citation, only the citekey is shown on the dialog
(by default, this is the node id).
