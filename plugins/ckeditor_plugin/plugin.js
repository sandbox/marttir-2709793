/**
 * A CKeditor plugin to insert Biblio citations as footnotes
 */

CKEDITOR.plugins.add( 'footnotes_citations',
{
    requires : [ 'fakeobjects', 'htmlwriter' ],
    init: function( editor )
    {
      CKEDITOR.addCss(
        'img.cke_citation' +
        '{' +
          'background-image: url(' + CKEDITOR.getUrl( this.path + 'images/cite_icon.png' ) + ');' +
          'background-position: center center;' +
          'background-repeat: no-repeat;' +
          'border: 2px solid #0C95D6;' +
          'border-radius: 4px;' +
          'padding: 0 1px;' +
          'margin-left: 4px;' +
          'width: 28px;' +
          'height: 12px;' +
        '}'
      );
      CKEDITOR.dialog.add('citationsDialog', function( editor ) {
        var loadElements = function( editor, selection, element ) {
          var content = null;
          var inline = null;
          var attr_value = null;

          element.editMode = true;
          content = element.getText();
          inline = (element.getName() == 'ibib') ? 'checked' : '';

          if ( content.length > 0 ) {
            this.setValueOf( 'info', 'citation', content );
            this.setValueOf( 'info', 'inline', inline );
          }
          else {
            this.setValueOf( 'info', 'citation', '' );
            this.setValueOf( 'info', 'inline', '' );
          }
        };

        var dialog = {
          title : Drupal.t('Editing citation'),
          minWidth : 500,
          minHeight : 50,
          contents : [
            {
              id : 'info',
              label : Drupal.t('Add a citation'),
              title : Drupal.t('Add a citation'),
              elements :
              [
                {
                  id : 'citation',
                  type : 'text',
                  className : 'ui-front',
                  label : Drupal.t('Select citation') + ':'
                },
                {
                  id: 'inline',
                  type: 'checkbox',
                  label: 'Inline citation'
                }
              ]
            }
          ],
          onShow : function() {
            this.editObj = false;
            this.fakeObj = false;
            this.editMode = false;

            var self = this;
            var selection = editor.getSelection();
            var ranges = selection.getRanges();
            var element = selection.getSelectedElement();
            var seltype = selection.getType();

            if ( seltype == CKEDITOR.SELECTION_ELEMENT
              && element.getAttribute( 'data-cke-real-element-type' )
              && (
                element.getAttribute( 'data-cke-real-element-type' ) == 'bib'
                || element.getAttribute( 'data-cke-real-element-type' ) == 'ibib'
              )
            ) {
              this.fakeObj = element;
              element = editor.restoreRealElement( this.fakeObj );
              loadElements.apply( this, [ editor, selection, element ] );
              selection.selectElement( this.fakeObj );
            }

            jQuery('input', editor.$).autocomplete({
              source: function (request, callback) {
                jQuery.post(
                  '/biblio/json',
                  {
                    q: request.term
                  },
                  function (data) {
                    data = jQuery.map(data, function (e,i) {
                      return {
                        value: i,
                        label: e
                      }
                    });
                    callback(data);
                  },
                  'json'
                )
              },
              minLength: 2,
              open: function() {
                var widget = jQuery(this).autocomplete('widget');
                widget.css({
                  // CKE dialog has z-index 10010, this needs to be above that
                  'z-index': 10011,
                  'border': '1px solid #aaa',
                  'width': 'auto'
                });
                widget.find('a').css({
                  'white-space': 'normal'
                });
              }
            });

            jQuery('input', editor.$).next('span').css({
              'display': 'none'
            });

            this.getContentElement( 'info', 'citation' ).focus();
          },
          onOk : function() {
            var editor = this.getParentEditor();
            var content = this.getValueOf( 'info', 'citation' );
            var inline = this.getValueOf( 'info', 'inline' );
            var type = (inline == true) ? 'ibib' : 'bib';

            if ( content.length > 0 ) {
              var realElement = CKEDITOR.dom.element.createFromHtml('<'+type+'></'+type+'>');
              realElement.setText(content);
              var fakeElement = editor.createFakeElement( realElement , 'cke_citation', type, false );
              editor.insertElement(fakeElement);
            }
          },
          onHide: function () {
            jQuery('input', editor.$).autocomplete('destroy');
          }
        };
        return dialog;
      }); // CKEDITOR.dialog.add

      editor.addCommand('footnotes_citations', new CKEDITOR.dialogCommand('citationsDialog'));
      // Drupal Wysiwyg requirement: The first argument to editor.ui.addButton()
      // must be equal to the key used in $plugins[<pluginName>]['buttons'][<key>]
      // in hook_wysiwyg_plugin().
      editor.ui.addButton('footnotes_citations',
        {
            label : Drupal.t('Add a citation'),
            icon : this.path + 'images/citations.png',
            command : 'footnotes_citations'
        });
      if ( editor.addMenuGroup ) {
        editor.addMenuGroup('footnotes_citations');
      }
      if ( editor.addMenuItems ) {
        editor.addMenuItems(
          {
            citations :
              {
                label : Drupal.t('Edit citation'),
                command : 'footnotes_citations',
                icon : this.path + 'images/citations.png',
                group : 'footnotes_citations'
              }
          });
      }
      if ( editor.contextMenu ) {
        editor.contextMenu.addListener(function(element, selection) {
          if (element.is( 'img' )) {
            if (element.getAttribute( 'data-cke-real-element-type' ) == 'bib'
              || element.getAttribute( 'data-cke-real-element-type' ) == 'ibib') {
              return { citations : CKEDITOR.TRISTATE_OFF };
            }
          }
          else {
            return null;
          }
        });
      }
    },
    afterInit : function( editor )
    {
      var dataProcessor = editor.dataProcessor,
        dataFilter = dataProcessor && dataProcessor.dataFilter;

      if ( dataFilter )
      {
        dataFilter.addRules(
          {
            elements :
              {
                'bib' : function( element )
                  {
                      var fakeElement = editor.createFakeParserElement(element, 'cke_citation', 'bib', false );
                      return fakeElement;
                  },
                'ibib' : function( element )
                  {
                      var fakeElement = editor.createFakeParserElement(element, 'cke_citation', 'ibib', false );
                      return fakeElement;
                  }
              }
          },
          5);
      }
    }
});
